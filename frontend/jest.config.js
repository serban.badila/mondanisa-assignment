module.exports = {
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/$1',
      '^~/(.*)$': '<rootDir>/$1',
      '^vue$': 'vue/dist/vue.common.js',
      'vuetify/lib(.*)': '<rootDir>/node_modules/vuetify/es5$1'
    },
    moduleFileExtensions: [
      'js',
      'vue',
      'json'
    ],
    transform: {
      '^.+\\.js$': 'babel-jest',
      '.*\\.(vue)$': 'vue-jest'
    },
    testEnvironment: 'jsdom',
    "setupFilesAfterEnv": ["<rootDir>/test/unit/setupTests.js"]
  }