
test-backend:
	cd backend && go test -v

test-frontend:
	cd frontend && npm run test


build:
	- docker-compose build

run: build
	- docker-compose up -d

stop:
	docker-compose down