package main

import (
	"fmt"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateTaskAlreadyExists(t *testing.T) {
	description := "attempting to insert a duplicate task is an invalid operation"

	// Fixtures
	dummyTask := "test task"
	initStore(IN_MEMORY)
	taskStore.insertOne(dummyTask)

	err := taskStore.insertOne(dummyTask)
	assert.NotEqualf(t, nil, err, description)
	assert.Equalf(t, fmt.Sprintf("Task \"%s\" already exists!", dummyTask), err.Error(), description)
}

func TestCanCreateTasks(t *testing.T) {
	description := "can insert and retrieve tasks"

	// Fixtures
	initialTask := "initial task"
	initStore(IN_MEMORY)
	taskStore.insertOne(initialTask)

	taskStore.insertOne("task1")
	taskStore.insertOne("task2")

	tasks := taskStore.getAll()
	sort.Slice(tasks, func(i, j int) bool {
		return tasks[i] < tasks[j]
	})
	expectedTasks := []string{initialTask, "task1", "task2"}

	assert.Equalf(t, expectedTasks, tasks, description)
}
