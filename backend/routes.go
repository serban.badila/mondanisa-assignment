package main

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func getAll(c *fiber.Ctx) error {
	taskList := taskStore.getAll()
	c.Append("Access-Control-Allow-Origin", fmt.Sprintf("http://localhost:%s", config["FRONTEND_PORT"]))
	return c.JSON(taskList)
}

func createTask(c *fiber.Ctx) error {
	payload := struct {
		Name string `json:"name"`
	}{}
	c.Append("Access-Control-Allow-Origin", fmt.Sprintf("http://localhost:%s", config["FRONTEND_PORT"]))

	if err := c.BodyParser(&payload); err != nil {
		return c.Status(500).JSON(err.Error())
	}
	err := taskStore.insertOne(payload.Name)
	if err != nil {
		return c.Status(409).JSON(err.Error())
	}
	return c.JSON("Task inserted!")
}

func options(c *fiber.Ctx) error {
	c.Append("Access-Control-Allow-Origin", fmt.Sprintf("http://localhost:%s", config["FRONTEND_PORT"]))
	c.Append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	c.Append("Access-Control-Allow-Methods", "POST, OPTIONS, GET")
	// c.Append("Access-Control-Allow-Headers", "content-type")
	return nil
}
