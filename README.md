# Modanisa Assignment

## Run tests

For the backend:

`make test-backend`

For the frontend:

`make test-frontend`


## Starting the app

To start up the whole app, including the database and the backend service execute the make command from the project's root directoy:

`make run`

By default, the frontend server listens on port `3001` and the backend on port `3000`. The three components are running inside a docker container network.

To stop the database and the bckend docker containers:

`make stop`

